/* Includes ------------------------------------------------------------------*/
#include "CANRoutines.h"

/* USER CODE BEGIN 0 */

CAN_FilterTypeDef     sFilterConfig;
CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;

void	CAN_Receive_Brake();
sMotorController MC;
SteeringWheel SW;
SolarCircuit SC;
BatteryManegmentSystem BMS;
DriverControls DC;
BMS_DiagnosticFlags BmsDFlags;
MC_DiagnosticFlags MCFlags;
MonitorCircuit MOC;
RelayBoard RB1;
RelayBoard RB2;

uint8_t               RX_Data[8];
uint32_t              TxMailbox;

uint16_t Steering_POSITION=0;
uint8_t Steering_frenection =0;
void CAN_Receive_Steering_MD(void);


group_64 canTempMess64;
group_32 canTempMess32;
/* USER CODE END 0 */
/* USER CODE BEGIN 1 */
void CAN_FilterandStartConfig(CAN_HandleTypeDef *hcan,uint32_t FilterBank) 
{
	
	/*##-2- Configure the CAN Filter ###########################################*/
  sFilterConfig.FilterBank = FilterBank;
  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
  sFilterConfig.FilterIdHigh = 0x0000;
  sFilterConfig.FilterIdLow = 0x0000;
  sFilterConfig.FilterMaskIdHigh = 0x0000;
  sFilterConfig.FilterMaskIdLow = 0x0000;
  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
  sFilterConfig.FilterActivation = ENABLE;
  sFilterConfig.SlaveStartFilterBank = FilterBank;
	
	if (HAL_CAN_ConfigFilter(hcan, &sFilterConfig) != HAL_OK)
  {
		/* Filter configuration Error */
    Error_Handler();
  }
	
	/*##-3- Start the CAN peripheral ###########################################*/
  if (HAL_CAN_Start(hcan) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
	
	 /*##-4- Activate CAN RX notification #######################################*/
 if (HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
 {
    /* Notification Error */
    Error_Handler();
  }

}

/* Parse Can Message Source    **********************************************

Author : Alper Dedeoglu
File: cancomroutines.c
Date: 16.06.2015
Caution: 
Input Parameter: void
Return Value: void 
Description: Decides the source of the CAN message and calls the individual 
parser function. 

ITU SOLAR CAR TEAM         ******************************************************/


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
		HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RX_Data);
	
	CAN_Receive_Brake();
		if(RxHeader.StdId <= 0x0100)
		{
				CAN_Receive_BMSDatas();
		}
		else   if((RxHeader.StdId >= 0x0400) && (RxHeader.StdId <= 0x0510))
		{
			  CAN_Receive_DCDatas();
				CAN_Receive_MCDatas();
			 
		}		
		else if ((RxHeader.StdId >= 0x0250) && (RxHeader.StdId <= 0x0270))
		{
				CAN_Receive_SCDatas();
		}
		else if ((RxHeader.StdId >= 0x0200) && (RxHeader.StdId <= 0x0210))
		{
				CAN_Receive_SWDatas();
		}
		else if ((RxHeader.StdId >= 0x0300) && (RxHeader.StdId <= 0x0350))
		{
				CAN_Receive_MOCDatas();
		}
//		else if ((RxHeader.StdId >= 0x0600) && (RxHeader.StdId <= 0x619))
//		{
//				CAN_Receive_RB1Datas();
//		}
//		else if ((RxHeader.StdId >= 0x0620) && (RxHeader.StdId <= 0x639))
//		{
//				CAN_Receive_RB2Datas();
//		}
	  CAN_Receive_Steering_MD();
		
}
 


void CAN_Receive_SCDatas()
{
		switch  (RxHeader.StdId - SC_CAN_BASE)
	{
		
			case SC_GROUP1_CURRENT:
					canTempMess64.data_u8[0] = RX_Data[0];
					canTempMess64.data_u8[1] = RX_Data[1];
					canTempMess64.data_u8[2] = RX_Data[2];
					canTempMess64.data_u8[3] = RX_Data[3];
					canTempMess64.data_u8[4] = RX_Data[4];
					canTempMess64.data_u8[5] = RX_Data[5];
					canTempMess64.data_u8[6] = RX_Data[6];
					canTempMess64.data_u8[7] = RX_Data[7];

					SC.Group1Current_fp =  canTempMess64.data_fp[0];  
					SC.Group2Current_fp =  canTempMess64.data_fp[1];		
//					MPPTGroup1Power = MPPTGroup1Current_Float * MC.BusVoltage;	
//					MPPTGroup2Power = MPPTGroup2Current_Float * MC.BusVoltage;
			break;

	
			case SC_TEMP:
					canTempMess64.data_u8[0] = RX_Data[0];
					canTempMess64.data_u8[1] = RX_Data[1];
					canTempMess64.data_u8[2] = RX_Data[2];
					canTempMess64.data_u8[3] = RX_Data[3];
					canTempMess64.data_u8[4] = RX_Data[4];
					canTempMess64.data_u8[5] = RX_Data[5];
					canTempMess64.data_u8[6] = RX_Data[6];
					canTempMess64.data_u8[7] = RX_Data[7];

					SC.MPPTTemp_fp =  canTempMess64.data_fp[0];   	
					SC.MPPTTemp = (uint8_t) SC.MPPTTemp;	
	   	break;
			
			
			case SC_TTDistance:
					canTempMess64.data_u8[0] = RX_Data[0];
					canTempMess64.data_u8[1] = RX_Data[1];
					canTempMess64.data_u8[2] = RX_Data[2];
					canTempMess64.data_u8[3] = RX_Data[3];
					canTempMess64.data_u8[4] = RX_Data[4];
					canTempMess64.data_u8[5] = RX_Data[5];
					canTempMess64.data_u8[6] = RX_Data[6];
					canTempMess64.data_u8[7] = RX_Data[7];

					SC.TTDistance = canTempMess64.data_u32[0];
	    break;
		}
}

void CAN_Receive_SWDatas()
{
		switch(RxHeader.StdId - SW_CAN_BASE)
		{
				case SW_DRIVE_MEAS: 
					
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];
										
						SW.ReferenceVelocity_fp = canTempMess64.data_u32[0];
						SW.M_ReferenceVelocity = (uint8_t) SW.ReferenceVelocity_fp;
						SW.Regenative_fp = canTempMess64.data_u32[1];
						SW.M_Regen = (uint16_t) SW.Regenative_fp;
						SW.Status = 1;
				
				break;

				case SW_DRIVE_FLAGS:
					
						SW.CC_Flag           = RX_Data[0];
						SW.CC_Negative_Flag  = RX_Data[1];
						SW.CC_Positive_Flag  = RX_Data[2];
						SW.Drive_Flag        = RX_Data[3];
						SW.Neutral_Flag      = RX_Data[4];
						SW.Reverse_Flag      = RX_Data[5];
						SW.RightSignal_Flag  = RX_Data[6];
						SW.LeftSignal_Flag   = RX_Data[7];
				
				break;

				case SW_OTHER_FLAGS:
						SW.Solar_Flag       = RX_Data[0];
						SW.Radio_Flag       = RX_Data[1];
						SW.Emergency_Flag   = RX_Data[2];
						SW.Controller_Flag  = RX_Data[5];		
				break;
				
				default:
				break;
		} 
}
void CAN_Receive_BMSDatas()
{ 
	
		switch(RxHeader.StdId - BMS_CAN_BASE)
		{
				case BMS_CAN_BASE:
						BMS.IgnitionStatus = RX_Data[0];	
				break;   
		
				case  BMS_CAN_CELL:
			      BMS.MinVoltagemV = RX_Data[0];
					  BMS.MaxVoltagemV = RX_Data[1];
						BMS.AverageVoltage = RX_Data[2];	
						BMS.MinVoltage  = (float) (((BMS.MinVoltagemV*10) + 2000)/1000.0f);
					  BMS.MaxVoltage = (float) (((BMS.MaxVoltagemV*10) + 2000)/1000.0f);	
				break;
		
				case BMS_ERROR_FLAGS:
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];
						BmsDFlags.NoCellCommunicationFlag    = GetBitValue( RX_Data[0],6);
						BmsDFlags.LeakageFlag                = GetBitValue( RX_Data[0],5);
						BmsDFlags.CellModuleOverHeatFlag     = GetBitValue( RX_Data[0],4);
						BmsDFlags.ChargeOverCurrentFlag      = GetBitValue( RX_Data[0],3);
						BmsDFlags.DischargeOverCurrentFlag   = GetBitValue( RX_Data[0],2);
						BmsDFlags.OverVoltageFlag            = GetBitValue( RX_Data[0],1);
						BmsDFlags.UnderVoltageFlag           = GetBitValue( RX_Data[0],0);
						if (BmsDFlags.NoCellCommunicationFlag)
						{
								BMS.ErrorCode=7;
						}
						else if (BmsDFlags.LeakageFlag)
						{
								BMS.ErrorCode=6;
						}
						else if (BmsDFlags.CellModuleOverHeatFlag)
						{
								BMS.ErrorCode=5;
						}
						else if (BmsDFlags.ChargeOverCurrentFlag)
						{
								BMS.ErrorCode=4;
						}
						else if (BmsDFlags.DischargeOverCurrentFlag)
						{
								BMS.ErrorCode=3;
						}
						else if (BmsDFlags.OverVoltageFlag)
						{
								BMS.ErrorCode=2;
						}
						else if (BmsDFlags.UnderVoltageFlag)
						{
								BMS.ErrorCode=1;
						}
						else
						{
								BMS.ErrorCode=0;
						}
        break;

				case BMS_CAN_TEMP_MODULE:
						BMS.MinTemp		  = RX_Data[0];
						BMS.MaxTemp 		= RX_Data[1];
						BMS.AverageTemp = RX_Data[2];	
				break;

				case BMS_SOC_CURRENT:
						BMS.CurrentMSB	  = RX_Data[0];
						BMS.CurrentLSB 		= RX_Data[1];
						BMS.EstimatedSOC  = RX_Data[6];
							
						BMS.Current = (canTempMess64.data_u8[0]<<8) | canTempMess64.data_u8[1];
						BMS.Current = (BMS.Current/10);
				break;
			
	       
	
				case 0x45:
						BMS.EventNumber = RX_Data[2];					
							//Most significant byte of the event data. If data type is 0 (Event info), this byte contains the event ID. List of event IDs: 
						//0 � No event;
						//1 � BMS Started;
						//2 � Lost communication to cells;
						//3 � Established communication to cells; 
						//4 � Cells voltage critically low;
						//5 � Critical low voltage recovered;
						//6 � Cells voltage critically high; 
						//7 � Critical high voltage recovered;
						//8 � Discharge current critically high;
						//9 � Discharge critical high current recovered;
						//10 � Charge current critically high;
						//11 � Charge critical high current recovered; 
						//12 � Cell module temperature critically high;
						//13 � Critical high cell module temperature recovered; 
						//14 � Leakage detected; 
						//15 � Leakage recovered;
						//16� Warning: Low voltage - reducing power;
						//17 � Power reduction due to low voltage recovered;
						//18� Warning: High current - reducing power;
						//19 � Power reduction due to high current recovered;
						//20 � Warning: High cell module temperature - reducing power;
						//21 � Power reduction due to high cell module temperature recovered;
						//22 � Charger connected;
						//23 � Charger disconnected;
						//24 � Started pre-heating stage;
						//25 � Started pre-charging stage;
						//26 � Started main charging stage;
						//27 � Started balancing stage;
						//28 � Charging finished; 
						//29 � Charging error occurred;
						//30 � Retrying charging;
						//31 � Restarting charging;
						//42 � Cell temperature critically high;
						//43 � Critically high cell temperature recovered; 
						//44 � Warning: High cell temperature � reducing power; 
						//45 � Power reduction due to high cell temperature recovered;		
				break;		
	
				case BMS_VOLTAGE_GROUP1:	
						BMS.Group1Cell[0] = RX_Data[0];
						BMS.Group1Cell[1] = RX_Data[1];
						BMS.Group1Cell[2] = RX_Data[2];
						BMS.Group1Cell[3] = RX_Data[3];
						BMS.Group1Cell[4] = RX_Data[4];
						BMS.Group1Cell[5] = RX_Data[5];
						BMS.Group1Cell[6] = RX_Data[6];
						BMS.Group1Cell[7] = RX_Data[7];
				break;
	
				case BMS_VOLTAGE_GROUP2:	
						BMS.Group2Cell[0] = RX_Data[0];
						BMS.Group2Cell[1] = RX_Data[1];
						BMS.Group2Cell[2] = RX_Data[2];
						BMS.Group2Cell[3] = RX_Data[3];
						BMS.Group2Cell[4] = RX_Data[4];
						BMS.Group2Cell[5] = RX_Data[5];
						BMS.Group2Cell[6] = RX_Data[6];
						BMS.Group2Cell[7] = RX_Data[7];
				break;
	
				case	BMS_VOLTAGE_GROUP3:
						BMS.Group3Cell[0] = RX_Data[0];
						BMS.Group3Cell[1] = RX_Data[1];
						BMS.Group3Cell[2] = RX_Data[2];
						BMS.Group3Cell[3] = RX_Data[3];
						BMS.Group3Cell[4] = RX_Data[4];
						BMS.Group3Cell[5] = RX_Data[5];
						BMS.Group3Cell[6] = RX_Data[6];
						BMS.Group3Cell[7] = RX_Data[7];
				break;
		
				case BMS_VOLTAGE_GROUP4:
						BMS.Group4Cell[0] = RX_Data[0];
						BMS.Group4Cell[1] = RX_Data[1];
						BMS.Group4Cell[2] = RX_Data[2];
						BMS.Group4Cell[3] = RX_Data[3];
						BMS.Group4Cell[4] = RX_Data[4];
						BMS.Group4Cell[5] = RX_Data[5];
						BMS.Group4Cell[6] = RX_Data[6];
						BMS.Group4Cell[7] = RX_Data[7];
				break;

				case	BMS_VOLTAGE_GROUP5:		
						BMS.Group5Cell[0] = RX_Data[0];
						BMS.Group5Cell[1] = RX_Data[1];
						BMS.Group5Cell[2] = RX_Data[2];
						BMS.Group5Cell[3] = RX_Data[3];
						BMS.Group5Cell[4] = RX_Data[4];
				break;    
	
				default:
				break; 
		}				
}
	
void CAN_Receive_MCDatas(void)
{
		MC.WheelRadius = 0.2795;	// Wheel radius.

		switch(RxHeader.StdId - MC_CAN_BASE)
		{
			
				case MC_ID:
						canTempMess32.data_u8[3] = RX_Data[7];
						canTempMess32.data_u8[2] = RX_Data[6];
						canTempMess32.data_u8[1] = RX_Data[5];
						canTempMess32.data_u8[0] = RX_Data[4];
						MC.SerialNumber = canTempMess32.data_u32;

						MC.TritiumID[0] =  RX_Data[0];
						MC.TritiumID[1] =  RX_Data[1];
						MC.TritiumID[2] =  RX_Data[2];
						MC.TritiumID[3] =  RX_Data[3];							
				break;

				case MC_BUS:
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.BusCurrent = canTempMess64.data_fp[1]; //Amp
						MC.BusVoltage = canTempMess64.data_fp[0]; //Volts
				break;

				case MC_STATUS:

						MC.Errors.MotorOverSpeedFlag      = GetBitValue( RX_Data[2],8);
						MC.Errors.DesaturationFaultFlag   = GetBitValue( RX_Data[2],7);
						MC.Errors.UnderVoltageLockOutFlag = GetBitValue( RX_Data[2],6);
						MC.Errors.ConfigReadErrorFlag     = GetBitValue( RX_Data[2],5);
						MC.Errors.WatchdogResetFlag       = GetBitValue( RX_Data[2],4);
						MC.Errors.BadMotorPositionFlag    = GetBitValue( RX_Data[2],3);
						MC.Errors.DC_BusOverVoltageFlag   = GetBitValue( RX_Data[2],2);
						MC.Errors.SoftwareOverCurrentFlag = GetBitValue( RX_Data[2],1);
						MC.Errors.HardwareOverCurrentFlag = GetBitValue( RX_Data[2],0);
				break;

				case MC_VELOCITY:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.VehicleVelocity_ms = canTempMess64.data_fp[1]; // m/s
				    MC.VehicleVelocity_kmh = canTempMess64.data_fp[1]*3.6f; // km/h
						MC.MotorVelocity   = canTempMess64.data_fp[0];   // rpm
						
				break;

				case MC_PHASE:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.PhaseACurrent = canTempMess64.data_fp[1]; 	// Amps RMS
						MC.PhaseBCurrent = canTempMess64.data_fp[0];   	// Amps RMS

				break;

				case MC_V_VECTOR:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.MotorVoltageVector.Real = canTempMess64.data_fp[1]; 	// Volts
						MC.MotorVoltageVector.Img = canTempMess64.data_fp[0];   // Volts
				break;

				case MC_I_VECTOR:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.MotorCurrentVector.Real = canTempMess64.data_fp[1]; 		// Amps
						MC.MotorCurrentVector.Img =  canTempMess64.data_fp[0];   	// Amps
				break;

				case MC_BEMF:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.MotorBackEMF.Real = canTempMess64.data_fp[1]; 	// Volts, always zero
						MC.MotorBackEMF.Img =  canTempMess64.data_fp[0];   	// Volts

				break;

				case MC_RAIL_1:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.VoltageRail15V = canTempMess64.data_fp[1]; 		// Volts
						MC.VoltageRail1V65 =  canTempMess64.data_fp[0];   	// Volts
							
				break;

				case MC_RAIL_2:

				//Not needed
				break;

				case MC_FAN:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.FanSpeed = canTempMess64.data_fp[1]; 	// rpm
						MC.FanDrive =  canTempMess64.data_fp[0];   	// percent

				break;

				case MC_TEMP1:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.HeatSinkTemp = canTempMess64.data_fp[1]; 	// celcius
						MC.MotorTemp =  canTempMess64.data_fp[0];   	// celcius
				break;
				
				case MC_TEMP2:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.AirInletTemp = canTempMess64.data_fp[1]; 	// celcius
						MC.ProcessorTemp =  canTempMess64.data_fp[0];   	// celcius
						

				break;

				case MC_TEMP3:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.AirOutletTemp = canTempMess64.data_fp[1]; 	// celcius
						MC.CapacitorTemp =  canTempMess64.data_fp[0];   	// celcius

				break;

				case MC_CUMULATIVE:

						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];

						MC.DCBusAmpsHour = canTempMess64.data_fp[1]; 	// Ah
						MC.Odometer =  (uint32_t)canTempMess64.data_fp[0];   		// m
				
				break;

				default:
				break; 


		}
}
void CAN_Receive_DCDatas(void)
{
		switch (RxHeader.StdId -  DC_CAN_BASE)
		{
				case DC_DRIVE:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
					
						DC.Ref_Velocity = canTempMess64.data_fp[0];
						DC.Ref_Current  = canTempMess64.data_fp[1];
					
						if ( DC.Ref_Velocity >= 10000)
						{
								DC.akim_modu = 1;
						}
						else
						{
								DC.akim_modu = 0;
						}
				break;
				default:
			  break;
		}
}

void CAN_Receive_MOCDatas(void)
{
		switch (RxHeader.StdId -  MOC_CAN_BASE)
		{
				case MOC_BUTTON_STATUS:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
					
						MOC.HeadlightFlag = canTempMess64.data_u8[0];
						MOC.CoolerFlag = canTempMess64.data_u8[1];
				break;
				
				default:
			  break;
		}
}
	
void CAN_Receive_RB1Datas(void)
{
		switch (RxHeader.StdId -  RB1_CAN_BASE)
	{
				case RB1_STATUS:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
				
						RB1.Status = canTempMess64.data_u8[0];
						
				break;
				
				case RB1_ALTIMETER_DATA:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
				
						RB1.Temperature = canTempMess64.data_u16[0];
						RB1.Pressure = canTempMess64.data_u16[1];
						RB1.Humidity = canTempMess64.data_u16[2];
					
				break;
				
				default:
			  break;
		}
	
}
void CAN_Receive_RB2Datas(void)
{
		switch (RxHeader.StdId -  RB2_CAN_BASE)
	{
				case RB2_STATUS:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
				
						RB2.Status = canTempMess64.data_u8[0];
						
				break;
				
				case RB2_ALTIMETER_DATA:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];
						canTempMess64.data_u8[3] = RX_Data[3];
						canTempMess64.data_u8[4] = RX_Data[4];
						canTempMess64.data_u8[5] = RX_Data[5];
						canTempMess64.data_u8[6] = RX_Data[6];
						canTempMess64.data_u8[7] = RX_Data[7];		
				
						RB2.Temperature = canTempMess64.data_u16[0];
						RB2.Pressure = canTempMess64.data_u16[1];
						RB2.Humidity = canTempMess64.data_u16[2];
					
				break;
				
				default:
			  break;
		}
	
}


void CAN_Receive_Steering_MD(void)
{
		switch (RxHeader.StdId - Steering_MD_CAN_BASE)
		{
				case 0:		
//						canTempMess64.data_u8[0] = RX_Data[0];
//						canTempMess64.data_u8[1] = RX_Data[1];
//						canTempMess64.data_u8[2] = RX_Data[2];	
				

					
//						desired_pos = canTempMess64.data_u16[0];
//				    fren = canTempMess64.data_u8[2];
//				    desired_encpos = (uint16_t)(desired_pos*encoder_step);
				
				break;
				
				
				
				default:
			  break;
		}
}


void CAN_Receive_Brake(void)
{
		switch (RxHeader.StdId - Brake_CAN_BASE)
		{
				case 0:		
						canTempMess64.data_u8[0] = RX_Data[0];
						canTempMess64.data_u8[1] = RX_Data[1];
						canTempMess64.data_u8[2] = RX_Data[2];	
				

					
						desired_pos = canTempMess64.data_u16[0];
				    fren = canTempMess64.data_u8[2];
				    desired_encpos = (uint16_t)(desired_pos*encoder_step);
				
				break;
				
				
				
				default:
			  break;
		}
}


void CAN_Recieve_TelemetryDatas(void)
{
}

/*void ResetMotorController (void)
{
  
			
			
			TxHeader.StdId		= DC_CAN_BASE + DC_RESET;
			TxHeader.IDE		= CAN_ID_STD; 
			TxHeader.RTR		= CAN_RTR_DATA;
			TxHeader.DLC		= 8;
			
			TX_Data[0] 	= 0;
			RX_Data[1] 	= 0;
			RX_Data[2] 	= 0;
			RX_Data[3] 	= 0;
			RX_Data[4] 	= 0;
			RX_Data[5] 	= 0;
			RX_Data[6] 	= 0;
			RX_Data[7] 	= 0;
				
			CAN_Transmit(CAN2, &DCCanMess);
}*/



