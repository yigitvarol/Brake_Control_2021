/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "can.h"
#include "i2s.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdint.h"
#include "usbd_cdc_if.h"
#include <string.h>
#include "CANRoutines.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
extern uint8_t RightErrorFlag;
extern uint8_t LeftErrorFlag;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define stop 0
#define full_duty 19
#define half_duty  9
#define quad_duty  4		
#define CCW  0
#define CW   1
#define K1   100
#define K2   10
#define fren_MASKBIT 0x00000010

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
const float encoder_step = 4.16;
int counter_a = 0;
int counter_b = 0;
int counter  = 0;
int fren = 0;
int a;

int conta = 0;
int keep = 0;
char rx_buffer[5];
uint8_t command_nevermind;
uint8_t command_fren;
uint8_t command_pos1;
uint8_t command_pos2;
uint8_t command_pos3;
uint16_t desired_pos;
uint16_t desired_encpos;
uint8_t rx_buf[6];
uint8_t buf_count = 0;
float rotate=0;
typedef struct {
	
		float Kp;
		float Ki;
		float Kd ;
		float p_error;
		float d_error ;
		float i_error ;
		float prev_cte;
		float pidError ;
		float error;
}PID;

uint16_t speed;

float current=0;


//uint16_t current_value = ;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


void PID_SET(PID *obj, float kp, float ki, float kd){
	obj->Kd = kd;
	obj->Ki = ki;
	obj->Kp = kp;
	obj->d_error=0;
	obj->i_error=0;
	obj->p_error=0;
	obj->prev_cte=0;
	obj->pidError=0;
	obj->error=0;

}

float CALCULATE_PID(PID *obj, float error){
	
	obj->error=error;
	obj->d_error=error-obj->prev_cte;
	obj->i_error+=error;
	obj->p_error=error;
	obj->prev_cte=error;
	obj->pidError= (obj->p_error * obj->Kp) + (obj->i_error * obj->Ki) + (obj->d_error * obj->Kd);
  return obj->pidError;
}
     








void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == GPIO_PIN_6)
	{
		
			if((TIM2->CR1 & fren_MASKBIT))
			{
				counter_a--;
			}
			else if(!(TIM2->CR1 & fren_MASKBIT))
			{
				counter_a++;
			}

		if (fren == 0)
		{
		if ((TIM2->CR1 & fren_MASKBIT) && counter_a > -desired_encpos )
		{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, desired_pos);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
		}
		else if ((TIM2->CR1 & fren_MASKBIT) && counter_a == -desired_encpos )
		{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
			conta = 0;
		}
		
		} 
		
		if (fren == 1)
		{
		if (!(TIM2->CR1 & fren_MASKBIT) && counter_a < desired_encpos )
		{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, desired_pos);
		}
		else if (!(TIM2->CR1 & fren_MASKBIT) && counter_a == desired_encpos )
		{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
			conta = 0;
		}
		
		}

	}
	
	
	
}



//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//	if(huart->Instance == USART2)
//	{
//		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);

//	}
//}

// 
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//		
//	if(huart->Instance == USART2)
//	{
//	/*
//	command_fren  = rx_buffer[0] - 48;
//	command_pos1 = rx_buffer[1] - 48;
//	command_pos2 = rx_buffer[2] - 48;
//	command_pos3 = rx_buffer[3] - 48;
//		*/
//		counter_a = 0;
//	}
//		
//}

uint8_t buffer[12]= "I'm alive \n";
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  float kp=0.5 ;
	float kd=0.2 ;
	float ki=0.01;
	PID PID_OBJ;
  PID_SET(&PID_OBJ,kp,ki,kd);
	
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2S3_Init();
  MX_SPI1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_USB_DEVICE_Init();
  MX_CAN2_Init();
  /* USER CODE BEGIN 2 */
	
	CAN_FilterandStartConfig(&hcan2,14);
	HAL_CAN_Start(&hcan2);
  HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO0_MSG_PENDING);
	
	HAL_TIM_Encoder_Start(&htim2,TIM_CHANNEL_ALL); 
	HAL_TIM_PWM_Start		(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start		(&htim1, TIM_CHANNEL_2);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
	

	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		
		//CDC_Transmit_FS(buffer, strlen((char *)buffer));
		HAL_Delay(50);

	//	HAL_UART_Receive_IT(&huart2, (uint8_t *)rx_buffer, 6);
	//	HAL_Delay(100);
		
//	  rotate =  CALCULATE_PID(&PID_OBJ, desired_encpos-current);
//		current += rotate;
//		desired_encpos=rotate;
		
		
		
		
		if ((fren == 0) && desired_encpos && !(LeftErrorFlag) )
		{
	
			
			//if(!conta) // enter just at the beginning 
			//{
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, desired_pos);
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
   			//keep = desired_encpos;
				//conta++;
				
			//}

		}
		
	 else	if ((fren == 1) && desired_encpos && !(RightErrorFlag) )
		{

			//if(!conta) // enter just at the beginning 
			//{
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, desired_pos);
				//keep = desired_encpos;
				//conta++;
				
			//}

		}
		else if (desired_encpos == 0 || (LeftErrorFlag || RightErrorFlag)){
			
			
			__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, stop);
			__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, stop);

		}
	
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV6;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
  PeriphClkInitStruct.PLLI2S.PLLI2SN = 50;
  PeriphClkInitStruct.PLLI2S.PLLI2SR = 2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
